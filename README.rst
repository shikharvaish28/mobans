coala-mobans
============= 
 
coala-moban is a collection of useful templates that are mainly used
by coala to have standardized configuration over all of the coala 
repositories.

These templates are applied using ``moban`` templating engine.

`Template Engine <https://en.wikipedia.org/wiki/Template_processor>`__ 

Usage
~~~~~

Syncing a coala repo
---------------------

In order to apply templates of this repo to any of the coala repository :

1. Create a parent folder with coala repos and coala-moban as subdirectory,
`i.e`, the structure should be:

::

    parent-directory
    ├── <coala-repo>
    ├── coala-mobans


2. Clone the coala-mobans repo.

    ``$ git clone https://gitlab.com/coala/mobans coala-mobans``

3. Create a ``virtualenv`` and install all dependencies from
   ``test-requirements.txt``.

4. **cd** into the root folder of any coala repository and run :

     ``$ moban``
     
This will automatically make all the necessary changes as mentioned in
the template files.

**Precaution**: The ``mobans`` repo should be named as ``coala-mobans``. This
is done automatically while cloning in the above format.

Syncing moban repo
-------------------

To sync the moban repository:

1. Clone the following repos:

- `pypi-mobans <https://github.com/moremoban/pypi-mobans>`__,
- `python-appveyor-demo <https://github.com/ogrisel/python-appveyor-demo/>`__,
- `gitignore <https://github.com/github/gitignore/>`__,
- `artwork <https://github.com/coala/artwork/>`__.

2. Put them in the following directory structure:

::

    parent-directory
    ├── pypi-mobans
    ├── python-appveyor-demo
    ├── gitignore
    ├── artwork
    ├── coala-mobans


3. **cd** into the mobans repository and inorder to pull in the changes run :

     ``$ moban``

4. Recreate the changes in mobans repository by once again running:

     ``$ moban``


moban
~~~~~~~~~~
moban is a cli command tool which uses the high performance template 
engine (JINJA2) for static text generation.

`moban <http://moban.readthedocs.io/en/latest/>`__ 

Jinja2
~~~~~~
Jinja2 is a full featured template engine for Python. It has full unicode 
support, an optional integrated sandboxed execution environment, widely 
used and BSD licensed. It is similar to the Django template engine.

`Jinja2 <http://jinja.pocoo.org/docs/2.10/>`__
 
